const [ name ] = Deno.args;
const TLDs = (await (await fetch('https://data.iana.org/TLD/tlds-alpha-by-domain.txt')).text()).split('\n').slice(1, -1);
console.log(TLDs
    .filter(TLD => name.toLowerCase().endsWith(TLD.toLowerCase()))
    .map(TLD => (`${name.slice(0, -TLD.length)}.${TLD}`).toLowerCase())
    .join('\n')
);