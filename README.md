# deno-domain-finder

[![CC0 licence - Public domain dedication](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)

[Deno](https://deno.land/) port of [domain-finder](https://git.kaki87.net/KaKi87/domain-finder).

Usage : `deno run --allow-net https://git.kaki87.net/KaKi87/deno-domain-finder/raw/branch/master/index.js <name>`

Example :
```bash
$ deno run --allow-net https://git.kaki87.net/KaKi87/deno-domain-finder/raw/branch/master/index.js nowplaying
nowplay.ing
nowplayi.ng
```

This project is released under the Creative Commons Zero 1.0 license, aka. public domain dedication.